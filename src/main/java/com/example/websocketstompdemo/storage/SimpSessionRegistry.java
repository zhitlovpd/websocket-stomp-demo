package com.example.websocketstompdemo.storage;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * StompSessionRegistry.
 *
 * @author Pavel_Zhitlov
 */
@Component
public class SimpSessionRegistry {

    Map<UUID, String> processIdSimpSessionIdMap = new ConcurrentHashMap<>();

    public void putSimpSessionId(UUID processId, String simpSessionId) {
        processIdSimpSessionIdMap.put(processId, simpSessionId);
    }

    public Optional<String> getSimpSessionId(UUID processId) {
        return Optional.of(processIdSimpSessionIdMap.get(processId));
    }

    public Optional<UUID> getProcessId(String simpSessionId) {
        return processIdSimpSessionIdMap.entrySet().stream()
                .filter(entry -> entry.getValue().equals(simpSessionId))
                .map(Map.Entry::getKey)
                .findFirst();
    }

    public void deleteSimpSessionId(String simpSessionId) {
        processIdSimpSessionIdMap.entrySet().stream()
                .filter(entry -> entry.getValue().equals(simpSessionId))
                .forEach(entry -> processIdSimpSessionIdMap.remove(entry.getKey()));
    }
}
