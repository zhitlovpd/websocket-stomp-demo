package com.example.websocketstompdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request.
 *
 * @author Pavel_Zhitlov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    private String name;
}
