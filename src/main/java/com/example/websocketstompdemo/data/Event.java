package com.example.websocketstompdemo.data;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

import java.util.UUID;

/**
 * Event.
 *
 * @author Pavel_Zhitlov
 */
@Getter
@ToString
public class Event<E> extends ApplicationEvent {

    private final E event;
    private final UUID processId;
    private final UUID messageId;


    public Event(E event, UUID processId, UUID messageId) {
        super(event);
        this.event = event;
        this.processId = processId;
        this.messageId = messageId;
    }
}
