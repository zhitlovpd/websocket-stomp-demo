package com.example.websocketstompdemo.resource;

import com.example.websocketstompdemo.data.Event;
import com.example.websocketstompdemo.dto.Request;
import com.example.websocketstompdemo.storage.SimpSessionRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.util.UUID;

import static com.example.websocketstompdemo.util.Constants.MESSAGE_ID;
import static org.springframework.messaging.simp.SimpMessageHeaderAccessor.SESSION_ID_HEADER;

/**
 * GreetingController.
 *
 * @author Pavel_Zhitlov
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class StompResource {

    private final SimpSessionRegistry simpSessionRegistry;
    private final ApplicationEventPublisher applicationEventPublisher;

    @MessageMapping("/input")
    public void getRequest(@Header(SESSION_ID_HEADER) String simpSessionId,
                           @Header(MESSAGE_ID) String messageId,
                           Request request) {
        log.debug("getRequest() - start: simpSessionId = {}, messageId = {}, request = {}", simpSessionId, messageId, request);

        UUID processId = simpSessionRegistry.getProcessId(simpSessionId).orElse(null);

        Event<Request> requestEvent = new Event<>(request, processId, UUID.fromString(messageId));
        applicationEventPublisher.publishEvent(requestEvent);
        log.debug("getRequest() - end: requestEvent = {}", requestEvent);
    }


    @EventListener
    public void onSessionConnectedEvent(SessionConnectedEvent event) {
        log.debug("onSessionConnectedEvent() - start: event = {}", event);
    }

    @EventListener
    public void onSessionSubscribeEvent(SessionSubscribeEvent event) {
        log.debug("onSessionSubscribeEvent() - start: event = {}", event);
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String processId = headers.getDestination().replace("/user/queue/", "");
        simpSessionRegistry.putSimpSessionId(UUID.fromString(processId), headers.getSessionId());
        log.info("onSessionSubscribeEvent() - processId = {}, session = {}", processId, headers.getSessionId());
    }

    @EventListener
    public void onSessionDisconnectedEvent(SessionDisconnectEvent event) {
        log.debug("onSessionDisconnectedEvent() - start: event = {}", event);
        simpSessionRegistry.deleteSimpSessionId(event.getSessionId());
    }
}
