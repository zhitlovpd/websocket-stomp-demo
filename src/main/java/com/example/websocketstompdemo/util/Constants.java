package com.example.websocketstompdemo.util;

public class Constants {

    public static final String PROCESS_ID = "process-id";
    public static final String MESSAGE_ID = "message-id";
    public final static String CORRELATION_ID = "correlation-id";
}
