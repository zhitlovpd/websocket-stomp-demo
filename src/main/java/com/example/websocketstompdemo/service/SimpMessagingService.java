package com.example.websocketstompdemo.service;

import com.example.websocketstompdemo.dto.Response;
import com.example.websocketstompdemo.storage.SimpSessionRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.example.websocketstompdemo.util.Constants.CORRELATION_ID;

/**
 * StompResponseSender.
 *
 * @author Pavel_Zhitlov
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SimpMessagingService {

    private final SimpSessionRegistry simpSessionRegistry;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public <R> void sendResponse(UUID processId, UUID correlationId, R response) {
        log.debug("sendResponse() - start: processId = {}, response = {}", processId, response);
        simpSessionRegistry.getSimpSessionId(processId).ifPresentOrElse(simpSessionId -> {
            SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
            headerAccessor.setSessionId(simpSessionId);
            headerAccessor.setNativeHeader(CORRELATION_ID, correlationId.toString());
            String destination = "/queue/".concat(processId.toString());
            simpMessagingTemplate.convertAndSendToUser(simpSessionId, destination, response, headerAccessor.getMessageHeaders());
            log.debug("sendResponse() - end: processId = {}, simpSessionId = {}, destination = {}", processId, simpSessionId, destination);
        }, () -> log.error("sendResponse() - simpSessionId not found by processId = {},", processId));
    }
}
