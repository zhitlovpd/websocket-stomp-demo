package com.example.websocketstompdemo.service;

import com.example.websocketstompdemo.data.Event;
import com.example.websocketstompdemo.dto.Request;
import com.example.websocketstompdemo.dto.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * ResponseEventService.
 *
 * @author Pavel_Zhitlov
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RequestEventService {

    private final SimpMessagingService simpMessagingService;

    @EventListener
    public void onHandle(Event<Request> requestEvent) {
        log.debug("onHandle() - start: requestEvent = {}", requestEvent);
        UUID processId = requestEvent.getProcessId();
        UUID messageId = requestEvent.getMessageId();
        Response response = new Response().setMessage(
                String.format("Hello, %s ! processId = %s, correlationId = %s", requestEvent.getEvent().getName(), processId, messageId));

        simpMessagingService.sendResponse(processId, messageId, response);
        log.debug("onHandle() - end: response = {}", response);
    }
}
