package com.example.websocketstompdemo.config;

import com.example.websocketstompdemo.interceptor.WebsocketHandshakeInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
import org.springframework.web.socket.sockjs.transport.handler.WebSocketTransportHandler;
import org.springframework.web.socket.sockjs.transport.handler.XhrReceivingTransportHandler;
import org.springframework.web.socket.sockjs.transport.handler.XhrStreamingTransportHandler;

/**
 * WebSocketConfig.
 *
 * @author Pavel_Zhitlov
 */
@Slf4j
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private final WebsocketHandshakeInterceptor websocketHandshakeInterceptor;

    public WebSocketConfig(WebsocketHandshakeInterceptor websocketHandshakeInterceptor) {
        this.websocketHandshakeInterceptor = websocketHandshakeInterceptor;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/queue");
        registry.setApplicationDestinationPrefixes("/app");
        AntPathMatcher pathMatcher = new AntPathMatcher();
        pathMatcher.setCachePatterns(false);
        registry.setPathMatcher(pathMatcher)
                .enableSimpleBroker()
                .setHeartbeatValue(new long[]{10000, 10000})
                .setTaskScheduler(new ConcurrentTaskScheduler());
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry
                .addEndpoint("/websocket")
//                .setAllowedOriginPatterns("*") // ignore cors
                .setAllowedOrigins("*")
                .withSockJS()
//                .setHeartbeatTime(1000)
                .setInterceptors(websocketHandshakeInterceptor)
                .setTransportHandlers(
                        new WebSocketTransportHandler(new DefaultHandshakeHandler()),
                        new XhrStreamingTransportHandler(),
                        new XhrReceivingTransportHandler());
    }
}
