package com.example.websocketstompdemo.it.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    @Autowired
    protected ObjectMapper objectMapper;

    @LocalServerPort
    private Integer port;

    protected String buildWsPath(String endpoint) {
        return String.format("ws://localhost:%d/".concat(endpoint), port);
    }
}
