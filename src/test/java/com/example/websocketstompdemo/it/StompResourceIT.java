package com.example.websocketstompdemo.it;

import com.example.websocketstompdemo.dto.Request;
import com.example.websocketstompdemo.dto.Response;
import com.example.websocketstompdemo.it.core.AbstractIntegrationTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static com.example.websocketstompdemo.util.Constants.CORRELATION_ID;
import static com.example.websocketstompdemo.util.Constants.MESSAGE_ID;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class StompResourceIT extends AbstractIntegrationTest {

    private WebSocketStompClient webSocketStompClient;

    @BeforeEach
    public void setup() {
        this.webSocketStompClient = new WebSocketStompClient(new SockJsClient(
                List.of(new WebSocketTransport(new StandardWebSocketClient()))));
        this.webSocketStompClient.setMessageConverter(new MappingJackson2MessageConverter());
    }

    @Test
    @SneakyThrows
    void getRequest() {
        String processId = UUID.randomUUID().toString();
        String messageId = UUID.randomUUID().toString();

        BlockingQueue<Pair<StompHeaders, Response>> blockingQueue = new ArrayBlockingQueue(1);

        StompSession session = webSocketStompClient
                .connect(buildWsPath("websocket"), new StompSessionHandlerAdapter() {})
                .get(10, SECONDS);

        session.subscribe("/user/queue/".concat(processId), new StompSessionHandlerAdapter() {
            @Override
            public Type getPayloadType(StompHeaders headers) {
                return Response.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                log.info("Received message: headers = {}, payload = {}", headers, payload);
                blockingQueue.add(Pair.of(headers, (Response) payload));
            }
        });

        //Given
        StompHeaders headers = new StompHeaders();
        headers.setDestination("/app/input");
        headers.add(MESSAGE_ID, messageId);

        Request request = new Request("Paul");

        //When
        session.send(headers, request);

        //Then
        Pair<StompHeaders, Response> pair = blockingQueue.poll(60, SECONDS);
        assertNotNull(pair);
        StompHeaders stompHeaders = pair.getLeft();
        Response response = pair.getRight();
        assertEquals("/user/queue/".concat(processId), stompHeaders.get("destination").get(0));
        assertEquals(messageId, stompHeaders.get(CORRELATION_ID).get(0));
        assertEquals(new Response(String.format("Hello, Paul ! processId = %s, correlationId = %s", processId, messageId)), response);
    }
}